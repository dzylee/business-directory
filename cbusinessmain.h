//file: CBusinessMain.h
//Class declaration for CBusinessMain. It is a Business Debtor Management system used to keep
//track of businesses that are owing money.  It is implemented to use a graphical user
//interface.


/*
(David) Zeng-Yuan LEE
Course: CMPT 212
Assignment #: 5
Date: April 4, 2003
*/


#ifndef _CBusinessMain_
#define _CBusinessMain_


#include "business.h"
#include "bizlist.h"


//module scope variable for specifying the number of BizList objects wanted for the program.
static const short numOfArrays = 8;


//##############################################################################################
class CBizDebtWnd : public CFrameWnd
//CBizDebtWnd class declaration.
{

public:


//==============================================================================================
   CBizDebtWnd();
   //Default constructor. Creates a CBizDebtWnd window.

//==============================================================================================
   ~CBizDebtWnd();
   //Destructor. Uses the default implementation (MFC handles destruction).


private:


//==============================================================================================
   BizList<Business> bizListArray[numOfArrays];
   //The array of BizList objects used by the program.

//==============================================================================================
   short workingList;
   //Stores the BizList object you want to perform operations on as a short value.

//==============================================================================================
   afx_msg void OnAddBusiness();
   //Event handler for click of the Add Business item under the List pull-down menu.

//==============================================================================================
   afx_msg void OnDisplayList();
   //Event handler for click of the Display List item under the List pull-down menu.

//==============================================================================================
   afx_msg void OnDisplayTotal();
   //Event handler for click of the Total Number item under the Miscellaneous pull-down menu.

//==============================================================================================
   afx_msg void OnAssignList();
   //Event handler for click of the Assign List item under the List pull-down menu.

//==============================================================================================
   afx_msg void OnEmptyList();
   //Event handler for click of the Empty List item under the List pull-down menu.

//==============================================================================================
   afx_msg void OnSaveList();
   //Event handler for click of the Save To File item under the File pull-down menu.

//==============================================================================================
   afx_msg void OnRestoreList();
   //Event handler for click of the Restore From File item under the File pull-down menu.

//==============================================================================================
   afx_msg void OnExit();
   //Event handler for click of the Exit item under the File pull-down menu.

//==============================================================================================
   afx_msg void OnPaint();
   //Redraws the contents of the CBizDebtWnd window.

//==============================================================================================
   DECLARE_MESSAGE_MAP()
   //Message map macro declaration.

};


#endif
