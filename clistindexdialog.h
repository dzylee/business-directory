//file: CListIndexDialog.h
//CListIndexDialog class declaration. Dialog box used to prompt the user for a BizList list
//selection for displaying or emptying.


/*
(David) Zeng-Yuan LEE
Course: CMPT 212
Assignment #: 5
Date: April 4, 2003
*/


#ifndef _CListIndexDialog_
#define _CListIndexDialog_


#include "CBusinessMain.h"


//##############################################################################################
class CListIndexDialog : public CDialog
//CListIndexDialog class declaration.
{

public:


//==============================================================================================
   CListIndexDialog(CBizDebtWnd *pParentWnd);
   //Constructor. Creates a CListIndexDialog. Pass in a pointer to the CBizDebtWnd object that
   //you want to be the parent window of the CListIndexDialog you are creating.

//==============================================================================================
   short listSelection;
   //Used to store the short value that the user supplies in the list selection field.


protected:


//==============================================================================================
   virtual BOOL OnInitDialog();
   //Initializes the CListIndexDialog object. Called automatically when a CListIndexDialog
   //object is creating.

//==============================================================================================
   afx_msg void OnOK();
   //Event handler for OK button clicks. Takes the value in the list selection field and stores
   //it in the listSelection member variable.

//==============================================================================================
   afx_msg void OnCancel();
   //Event handler for Cancel button clicks. Closes the dialog box and ignores whatever values
   //might be in the list selection field.

//==============================================================================================
   DECLARE_MESSAGE_MAP()
   //Message map macro declaration.

};


#endif
