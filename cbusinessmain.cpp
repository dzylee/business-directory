//file: CBusinessMain.cpp
//File contains implementation of CBizDebtWnd class, and the declaration and implementation of
//CBizDebtApp. CBizDebtApp is the Business Debtor Management system used to keep track of
//businesses that are owing money.  It is implemented to use a graphical user interface.


/*
(David) Zeng-Yuan LEE
Course: CMPT 212
Assignment #: 5
Date: April 4, 2003
*/


#include <afxwin.h>

#include <strstream>
using namespace std;

#include "business.h"
#include "bizlist.h"
#include "control.h"
#include "menuIDs.h"
#include "CListIndexDialog.h"
#include "CAddBusinessDialog.h"
#include "CBusinessMain.h"


static bool dispListReq = false;
static short currentlyShownList = -1;


//##############################################################################################
//                               CBizDebtWnd class implementation
//##############################################################################################


//==============================================================================================
CBizDebtWnd::CBizDebtWnd()
//Default constructor. Creates a CBizDebtWnd window on the desktop.
{
   this->Create(
      NULL,
      "Business Debtor Management by DZYL", //title.
      WS_OVERLAPPEDWINDOW, //full-featured window.
      CRect(150, 150, 800, 600), //location and dimensions.
      NULL, //contained immediately within desktop.
      "BIZFUNCTIONS" //using BIZFUNCTIONS menu.
      );
}

//==============================================================================================
CBizDebtWnd::~CBizDebtWnd()
//Destructor. Uses the default implementation (MFC handles destruction).
{
}

//==============================================================================================
void CBizDebtWnd::OnAddBusiness()
//Event handler for click of the Add Business item under the List pull-down menu.
{
   CAddBusinessDialog myDlg(this); //create a CAddBusinessDialog dialog.

   //if the user clicked the OK button.
   if( myDlg.DoModal() == IDOK )
   {
      workingList = myDlg.listSelection; //get the list you want to add business to.

      //adding MonthlyPayingBusiness.
      if( myDlg.MPBchecked == 1 )
      {
         MonthlyPayingBusiness *addMe = new MonthlyPayingBusiness(); //create MPB object.
         addMe->setID( myDlg.bizID ); //set the ID.
         addMe->setDebt( myDlg.amountOwing ); //set the debt.
         addMe->setMonthlyPaymentTo( myDlg.monthlyPayments ); //set the monthly payment.

         bizListArray[workingList].push_back( addMe ); //add the MPB.
      }
      //adding Business.
      else
      {
         Business *addMe = new Business(); //create Business object.
         addMe->setID( myDlg.bizID ); //set the ID.
         addMe->setDebt( myDlg.amountOwing ); //set the debt.

         bizListArray[workingList].push_back( addMe ); //add the Business.
      }
   }

   //update the display if you added a business to the currently showing display.
   if( currentlyShownList == workingList )
   {
      dispListReq = true; //set display requested variable to true.
      Invalidate(); //call OnPaint to redraw window.
   }
   dispListReq = false;
}

//==============================================================================================
void CBizDebtWnd::OnDisplayList()
//Event handler for click of the Display List item under the List pull-down menu.
{
   CListIndexDialog myDlg(this); //create a CListIndexDialog dialog.

   //if the user clicked the OK button.
   if( myDlg.DoModal() == IDOK )
   {
      workingList = myDlg.listSelection; //get the list you want to display.
      currentlyShownList = workingList; //set the currently showing list to the list about to
                                //be displayed.
      dispListReq = true; //set display requested variable to true.
      Invalidate(); //call OnPaint to redraw window.
   }
   dispListReq = false;
}

//==============================================================================================
void CBizDebtWnd::OnDisplayTotal()
//Event handler for click of the Total Number item under the Miscellaneous pull-down menu.
{
   char lineOfText[40]; //create string.
   ostrstream winOut( lineOfText, sizeof (lineOfText) ); //make new style I/O output stream.
   winOut.seekp(0);

   //set the string with the text you want to display in message box.
   winOut << "Total numbers: B=" << Business::getInstanceCount() << ", MPB="
      << MonthlyPayingBusiness::getInstanceCount() << ends;

   //display message box.
   MessageBox(
      lineOfText, //display the line of text as the message.
      "", //no caption.
      MB_ICONSTOP | MB_OK | MB_DEFBUTTON1 //use stop icon, OK button and cancel button.
   );
}

//==============================================================================================
void CBizDebtWnd::OnAssignList()
//Event handler for click of the Assign List item under the List pull-down menu.
{
   //display message box.
   MessageBox(
      "Command not implemented yet!", //message to display.
      "", //no caption
      MB_ICONSTOP | MB_OK | MB_DEFBUTTON1 //use stop icon, OK button and cancel button.
   );
}

//==============================================================================================
void CBizDebtWnd::OnEmptyList()
//Event handler for click of the Empty List item under the List pull-down menu.
{
   CListIndexDialog myDlg(this); //create a CListIndexDialog dialog.

   //if the user clicked the OK button.
   if( myDlg.DoModal() == IDOK )
   {
      workingList = myDlg.listSelection; //get the list you want to display.
      bizListArray[workingList].empty(); //empty the specified list.

      //if emptying the list that's currently showing.
      if( currentlyShownList == workingList )
      {
         dispListReq = true; //set display requested variable to true.
         Invalidate(); //call OnPaint to redraw window.
      }
   }
   dispListReq = false;
}

//==============================================================================================
void CBizDebtWnd::OnSaveList()
//Event handler for click of the Save To File item under the File pull-down menu.
{
   //display message box.
   MessageBox(
      "Command not implemented yet!", //message to display.
      "", //no caption
      MB_ICONSTOP | MB_OK | MB_DEFBUTTON1 //use stop icon, OK button and cancel button.
   );
}

//==============================================================================================
void CBizDebtWnd::OnRestoreList()
//Event handler for click of the Restore From File item under the File pull-down menu.
{
   //display message box.
   MessageBox(
      "Command not implemented yet!", //message to display.
      "", //no caption
      MB_ICONSTOP | MB_OK | MB_DEFBUTTON1 //use stop icon, OK button and cancel button.
   );
}

//==============================================================================================
void CBizDebtWnd::OnExit()
//Event handler for click of the Exit item under the File pull-down menu.
{
   SendMessage( WM_CLOSE ); //terminate the program.
}


//==============================================================================================
void CBizDebtWnd::OnPaint()
//Redraws the contents of the CBizDebtWnd window.
{
   CPaintDC dc(this); //create a device context for the window client area.
   CRect drawingArea; //create a rectangle object to store dimensions of client area.
   GetClientRect(&drawingArea); //get the dimensions of the client area.

   CFont myFont; //create a CFont variable to store font settings.
   myFont.CreatePointFont( 120, _T("Courier New"), &dc ); //define font settings.
   dc.SelectObject(&myFont); //activate those font settings.

   static char listShown[8]; //create a variable to hold "List #".
   static ostrstream winOut( listShown, sizeof(listShown) ); //make it a new style out-stream.
   winOut.seekp(0); //reset point to first character in string.

//if( dispListReq )

   //if display list requested.
   if( !(currentlyShownList < 0) )
   {
      winOut << "List " << currentlyShownList << ":" << ends; //set string to "List #".
      dc.TextOut( 10, 10, listShown ); //output the string to window.
      displayList( bizListArray[currentlyShownList], dc ); //display the contents of the list.
   }
}

//==============================================================================================
BEGIN_MESSAGE_MAP(CBizDebtWnd, CFrameWnd)
//Map the event handlers to their corresponding messages.
   ON_COMMAND( IDM_ADD_BUSINESS, OnAddBusiness ) //Add Business item clicked.
   ON_COMMAND( IDM_DISPLAY_LIST, OnDisplayList ) //Display List item clicked.
   ON_COMMAND( IDM_DISPLAY_TOTAL, OnDisplayTotal ) // Total Number item clicked.
   ON_COMMAND( IDM_ASSIGN_LIST, OnAssignList ) //Assign List item clicked.
   ON_COMMAND( IDM_EMPTY_LIST, OnEmptyList ) //Empty List item clicked.
   ON_COMMAND( IDM_SAVE_LIST, OnSaveList ) //Save To File item clicked.
   ON_COMMAND( IDM_RESTORE_LIST, OnRestoreList ) //Restore From File item clicked.
   ON_COMMAND( IDM_EXIT, OnExit ) //Exit item clicked.
   ON_WM_PAINT() //redraw the window.
END_MESSAGE_MAP()


//##############################################################################################
//                        CBizDebtApp class declaration and implementation
//##############################################################################################


class CBizDebtApp : public CWinApp
//CBizDebtApp class declaration and implementation.
{

public:


//==============================================================================================
   virtual BOOL InitInstance()
   //Initializes the CBizDebtWnd application. Called automatically by the operating system at
   //execution.
   {
      m_pMainWnd = new CBizDebtWnd(); //create an instance of CBizDebtWnd window.
      m_pMainWnd->ShowWindow( m_nCmdShow ); //display the window.
      m_pMainWnd->UpdateWindow(); //update all windows.
      return TRUE;
   }
};

CBizDebtApp myBizDebtApp; //create an instance of the application to be run.


//##############################################################################################
//                                        QUESTIONS
//##############################################################################################
/*

(1)

I like menu architecture B more than the others because I felt the functions are organized
into more logical groupings. I also like it because it doesn't have any menu headers that are
menu item leaves themselves (ie: in architecture A and C, the pull down menu 'Exit' is a
function in itself). There are two things I dislike about architecture B.  The first is the
placement of Total_Number item. I felt it was not a good decision to place it in a menu called
Miscellaneous since this functionality is very much an importance function of this program.
Second, I disliked the use of the Add submenu. The two types of business differ by only one
attribute, and I felt that one dialog box should be used for adding both types of businesses.
In my implementation, I removed the submenu and changed 'Add' to 'Add Business'. When the user
selects this option, he/she can decide whether the business to be added is of type Business or
MonthlyPayingBusiness using a checkbox provided in my add business dialog box.

(2)

"the CLASS THAT THE LIST HOLDS must meet certain criterion"

That means that the Business and MonthlyPayingBusiness classes must both have their own
contructors and destructors for creating and destroying objects of their respective types.

I suppose that makes sense since the templatized BizList class is 'generic', and it can't be
expected to know how to create and destroy all the various types of objects that it can store.

*/
