//file: business.h
//Contains Business and MonthlyPayingBusiness class declarations and function prototypes. Both
//classes are used to create objects that model a business record. Business objects store a
//business' identification number and the amount of debt owing. MonthlyPayingBusiness inherits
//from Business, and includes an additional attribute for storing the monthly payment being
//made by a business.


/*
(David) Zeng-Yuan LEE
Course: CMPT 212
Assignment #: 5
Date: April 4, 2003
*/


#ifndef _business_
#define _business_


#include <strstream>
using namespace std;


//##############################################################################################
class Business
//Business class declaration.
{

public:


//==============================================================================================
Business();
//Default constructor. Creates a Business record with business ID number and debt set to 0. Use
//setID() and setDebt() functions to change the ID and debt respectively.

//==============================================================================================
Business(const Business &cloneMe);
//Copy constructor. Creates a copy of a Business object. The object to be copied is passed in as
//a parameter via 'cloneMe'.

//==============================================================================================
virtual ~Business();
//Destructor. Automatically called when delete operator is used on a Business object to
//deallocate the memory being occupied by that object.

//==============================================================================================
unsigned long getID() const;
//Returns the business' ID number.

//==============================================================================================
float getDebt() const;
//Returns the business' owing debt.

//==============================================================================================
static unsigned long getInstanceCount();
//Static method that returns the total number of Business objects that currently exist.

//==============================================================================================
void setID(unsigned long id);
//Sets the ID of a business by passing in the new ID via 'id' parameter. ID numbers can only
//assume positive integer values of up to 9 digits long.

//==============================================================================================
void setDebt(float debt);
//Sets the owing debt of a business by passing in the new debt via 'debt' parameter. Maximum
//amount a business can owe is $9999.99. A business may not owe a negative debt.

//==============================================================================================
friend ostrstream & operator<<(ostrstream &os, const Business &printMe);
//Overload of the << operator. Enables output of a Business object to an output stream the same
//way a string can be outputed via '<<'.

//==============================================================================================
virtual void print(ostrstream &os);
//Enables output of a Business object to an output stream the same way a string can be outputed
//via '<<'. This function performs the same function as operator<<(), and it also outputs the
//Business in the exact same format as operator<<(). The difference is that a Business object
//must call this function using the membership operator, '.'. You also have to specify where you
//want to output the Business object to by passing in an output stream parameter to the function
//via 'os'.

//==============================================================================================
virtual Business * makePtrToClone();
//Creates a duplicate of the Business object that calls this function, and it returns a pointer
//of type Business to the newly created Business object.


protected:


//==============================================================================================
unsigned long bizID;
//Stores the business' ID number as an unsigned long value.

//==============================================================================================
float bizDebt;
//Stores the business' debt as a float value.

//==============================================================================================
static unsigned long instanceCount;
//Static variable that stores the total number of Business objects that currently exist.

};

//==============================================================================================
ostrstream & operator<<(ostrstream &os, const Business &printMe);
//Make the overloaded output operator with global scope so that you can use it to output
//Business objects from anywhere.


//##############################################################################################
class MonthlyPayingBusiness : public Business
//MonthlyPayingBusiness class declaration. Inherits from Business class. Inherited attributes
//are bizID and bizDebt. Inherited functions are getID(), getDebt(), setID(), and setDebt().
//the operator<<() function is also inherited, but using it to output a MonthlyPayingBusiness
//object will only display the inherited attributes of the object.
{

public:


//==============================================================================================
MonthlyPayingBusiness();
//Default constructor. Creates a MonthlyPayingBusiness object with business ID number, debt, and
//monthly payment set to 0. Use setID(), setDebt() and setMonthlyPaymentTo() functions to change
//the ID, debt, and monthly payment respectively.

//==============================================================================================
MonthlyPayingBusiness(const MonthlyPayingBusiness &cloneMe);
//Copy constructor. Creates a copy of a MonthlyPayingBusiness object. The object to be copied is
//passed in as a parameter via 'cloneMe'.

//==============================================================================================
virtual ~MonthlyPayingBusiness();
//Destructor. Automatically called when delete operator is used on a MonthlyPayingBusiness
//object to deallocate the memory being occupied by that object.

//==============================================================================================
static unsigned long getInstanceCount();
//Static method that returns the total number of MonthlyPayingBusiness objects that currently
//exist.

//==============================================================================================
float getMonthlyPayment() const;
//Returns the business' monthly payment amount.

//==============================================================================================
void setMonthlyPaymentTo(float amount);
//Sets the monthly payment of a business by passing in the new amount via 'amount' parameter.
//Maximum monthly payment a business can make is $9999.99.

//==============================================================================================
virtual void print(ostrstream &os);
//Enables output of a MonthlyPayingBusiness object to an output stream. You have to specify
//where you want to output the MonthlyPayingBusiness object to by passing in an output stream
//parameter to the function via 'os'.

//==============================================================================================
virtual Business * makePtrToClone();
//Creates a duplicate of the MonthlyPayingBusiness object that calls this function, and it
//returns a pointer of type Business to the newly created MontlyPayingBusiness object.


protected:


//==============================================================================================
float monthlyPayment;
//Stores the business' monthly payment amounts as a float value.

//==============================================================================================
static unsigned long instanceCount;
//Static variable that stores the total number of MonthlyPayingBusiness objects as an unsigned
//long value.

};


#endif
