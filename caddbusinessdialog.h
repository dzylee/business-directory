//file: CAddBusinessDialog.h
//CAddBusinessDialog class declaration. Dialog used to prompt user for a business' ID, amount
//owing, monthly payment amount (if it's a monthly paying business), and what list to add the
//business to. It also has a check for indicating if it's a Business object or a
//MonthlyPayingBusiness object.


/*
(David) Zeng-Yuan LEE
Course: CMPT 212
Assignment #: 5
Date: April 4, 2003
*/


#ifndef _CAddBusinessDialog_
#define _CAddBusinessDialog_


#include "CBusinessMain.h"


//##############################################################################################
class CAddBusinessDialog : public CDialog
//CAddBusinessDialog class declaration.
{

public:


//==============================================================================================
   CAddBusinessDialog(CBizDebtWnd *pParentWnd);
   //Constructor. Creates a CAddBusinessDialog. Pass in a pointer to the CBizDebtWnd object
   //that you want to be the parent window of the CAddBusinessDialog you are creating.

//==============================================================================================
   short listSelection;
   //Used to store the short value that the user supplies in the list selection field.

//==============================================================================================
   short bizID;
   //Used to store the short value that the user supplies in the business ID field.

//==============================================================================================
   float amountOwing;
   //Used to store the float value that the user supplies in the amount owing field.

//==============================================================================================
   float monthlyPayments;
   //Used to store the float value that the user supplies in the monthly payment field.

//==============================================================================================
   short MPBchecked;
   //Used to store the checked/unchecked status of the MPB checkbox.


protected:


//==============================================================================================
   virtual BOOL OnInitDialog();
   //Initializes the CAddBusinessDialog object. Called automatically when a CAddBusinessDialog
   //object is creating.

//==============================================================================================
   afx_msg void OnOK();
   //Event handler for OK button clicks. Takes the value in all the fields of the dialog and
   //stores them in the appropriate member variables of the dialog box.

//==============================================================================================
   afx_msg void OnCancel();
   //Event handler for Cancel button clicks. Closes the dialog box and ignores whatever values
   //might be in the information fields.

//==============================================================================================
   DECLARE_MESSAGE_MAP()
   //Message map macro declaration.

};


#endif
