//file: CAddBusinessDialog.cpp
//Implementation for CAddBusinessDialog. CAddBusinessDialog inherits from CDialog. Used to
//prompt the user for information related to the business to be stored into the system.


/*
(David) Zeng-Yuan LEE
Course: CMPT 212
Assignment #: 5
Date: April 4, 2003
*/


#include <afxwin.h>
#include "menuIDs.h"
#include "CAddBusinessDialog.h"
#include "CBusinessMain.h"


//==============================================================================================
CAddBusinessDialog::CAddBusinessDialog(CBizDebtWnd *pParentWnd)
   : CDialog(ADDBIZDIALOG, pParentWnd) //WHERE DID pParentWnd COME FROM???
                              //AND WHY DO I HAVE TO PASS IT AS A PARAMTER???
//Constructor. Creates a CAddBusinessDialog dialog box as a child of a CBizDebtWnd window. Pass
//in the 'this' parameter of the CBizDebtWnd parent window as the single parameter for this
//constructor.
{
}

//==============================================================================================
BOOL CAddBusinessDialog::OnInitDialog()
//Initializes the dialog box by chaining to a function by the same name inherited from the base
//class CDialog.
{
   CDialog::OnInitDialog();
   return TRUE;
}

//==============================================================================================
void CAddBusinessDialog::OnOK()
//Retrieves and stores the values held by all the controls in the dialog box and stores them in
//the appropriate member variables of the CAddBusinessDialog object.
{
   listSelection = GetDlgItemInt( IDC_ADDTOLISTNUMBER ); //get list to store to.
   bizID = GetDlgItemInt( IDC_IDNUMBER ); //get the business' ID number.

   char owing[7]; //string for storing the amount owing value of the business.
   istrstream winIn1(owing); //create new style I/O in stream.
   GetDlgItemText( IDC_OWING, owing, sizeof owing ); //get amount owing value.
   winIn1 >> amountOwing; //store amount owing value.

   char monthly[7]; //string for storing the monthly payment value of the business.
   istrstream winIn2(monthly); //create new style I/O in-stream.
   GetDlgItemText( IDC_PAYMENTS, monthly, sizeof monthly ); //get monthly paying value.
   winIn2 >> monthlyPayments; //create new style I/O in-stream.

   CButton *pCheckBox = (CButton *)GetDlgItem( IDC_CHECK_MPB ); //variable for storing check
                                                 //box status.
   MPBchecked = pCheckBox->GetCheck(); //get check box status.

   CDialog::OnOK(); //chain to OnOK in CDialog.
}

//==============================================================================================
void CAddBusinessDialog::OnCancel()
//Closes the dialog box and disregards any values that might be held in the controls of the
//dialog box.
{
   CDialog::OnCancel(); //chain to OnCancel in CDialog.
}

//==============================================================================================
BEGIN_MESSAGE_MAP(CAddBusinessDialog, CDialog)
//Map the event handlers to their corresponding messages.
   ON_COMMAND( IDC_OKADD, OnOK ) //OK button press.
   ON_COMMAND( IDC_CANCELADD, OnCancel ) //Cancel button press.
END_MESSAGE_MAP()


