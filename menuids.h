//file: menuIDs.h


/*
(David) Zeng-Yuan LEE
Course: CMPT 212
Assignment #: 5
Date: April 4, 2003
*/


//BIZFUNCTIONS IDs
#define IDM_ADD_BUSINESS   2200
#define IDM_DISPLAY_LIST   2201
#define IDM_DISPLAY_TOTAL   2202
#define IDM_ASSIGN_LIST      2203
#define IDM_EMPTY_LIST      2204
#define IDM_SAVE_LIST      2205
#define IDM_RESTORE_LIST   2206
#define IDM_EXIT         2207

//SELECTLISTDIALOG IDs
#define SELECTLISTDIALOG   2208
#define IDC_LISTNUMBER      2209
#define IDC_OKDISPLAY      2210
#define IDC_CANCELDISPLAY   2211

//ADDBIZDIALOG IDs
#define ADDBIZDIALOG      2212
#define IDC_ADDTOLISTNUMBER 2213
#define IDC_IDNUMBER      2214
#define IDC_OWING         2215
#define IDC_PAYMENTS      2216
#define IDC_CHECK_MPB      2217
#define IDC_OKADD         2218
#define IDC_CANCELADD      2219
