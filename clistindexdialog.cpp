//file: CListIndexDialog.cpp
//Implementation for CListIndexDialog. CListIndexDialog inherits from CDialog. Used to prompt
//the user for a BizList list selection for displaying or emptying.


/*
(David) Zeng-Yuan LEE
Course: CMPT 212
Assignment #: 5
Date: April 4, 2003
*/


#include <afxwin.h>
#include "menuIDs.h"
#include "CListIndexDialog.h"
#include "CBusinessMain.h"


//==============================================================================================
CListIndexDialog::CListIndexDialog(CBizDebtWnd *pParentWnd)
   : CDialog(SELECTLISTDIALOG, pParentWnd)
//Constructor. Creates a CListIndexDialog dialog box as a child of a CBizDebtWnd window. Pass in
//the 'this' parameter of the CBizDebtWnd parent window as the single parameter for this
//constructor.
{
}

//==============================================================================================
BOOL CListIndexDialog::OnInitDialog()
//Initializes the dialog box by chaining to a function by the same name inherited from the base
//class CDialog.
{
   CDialog::OnInitDialog();
   return TRUE;
}

//==============================================================================================
void CListIndexDialog::OnOK()
//Retrieves and stores the value in the list selection field at the time of the button press.
{
   listSelection = GetDlgItemInt( IDC_LISTNUMBER ); //store the value in the field.
   CDialog::OnOK(); //chain to OnOK in CDialog.
}

//==============================================================================================
void CListIndexDialog::OnCancel()
//Closes the dialog box and disregards any value that might be in the list selection field.
{
   CDialog::OnCancel(); //chain to OnCancel in CDialog.
}

//==============================================================================================
BEGIN_MESSAGE_MAP(CListIndexDialog, CDialog)
//Map the event handlers to their corresponding messages.
   ON_COMMAND( IDC_OKDISPLAY, OnOK ) //OK button press.
   ON_COMMAND( IDC_CANCELDISPLAY, OnCancel ) //Cancel button press.
END_MESSAGE_MAP()
