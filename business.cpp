//file: business.cpp
//Business and MonthlyPayingBusiness class implementations. Both data types are used to store
//information related how much debt a business owes. The inherited class, MonthlyPayingBusiness,
//includes an extra attribute for storing the amount a business is paying in monthly payments.


/*
(David) Zeng-Yuan LEE
Course: CMPT 212
Assignment #: 5
Date: April 4, 2003
*/


#include <strstream>
using namespace std;

#include "business.h"


//##############################################################################################
//                                 Business Class Implementation
//##############################################################################################


//==============================================================================================
unsigned long Business::instanceCount = 0;
//Initializes the static variable, instanceCount, to 0 before any Business objects are created.
//Used to keep track of how many instances of Business objects exist.

//==============================================================================================
Business::Business()
//Default constructor for Business class. Initializes business' ID number and debt to 0, and
//increments the static variable, instanceCount.
{
   bizID = 0;
   bizDebt = 0;
   instanceCount++;
}

//==============================================================================================
Business::Business(const Business &cloneMe)
//Copy contructor. Creates a duplicate of the Business object passed in via 'cloneMe' parameter.
//It also increments the static variable, instanceCount, since a new Business object is being
//created. Then it automatically returns a pointer to the newly created Business object.
{
   bizID = cloneMe.bizID;
   bizDebt = cloneMe.bizDebt;
   instanceCount++;
}

//==============================================================================================
Business::~Business()
//Destructor. Automatically called whenever deallocating the memory being occupied by a Business
//object. It also decrements the static variable, instanceCount.
{
   instanceCount--;
}

//==============================================================================================
unsigned long Business::getID() const
//Returns the ID number of the business as an unsigned long value.
{
   return (this->bizID);
}

//==============================================================================================
float Business::getDebt() const
//Returns the owing debt of the business as a float value.
{
   return (this->bizDebt);
}

//==============================================================================================
unsigned long Business::getInstanceCount()
//Static function that returns the number of Business objects that currently exist as as
//unsigned long value.
{
   return instanceCount;
}

//==============================================================================================
void Business::setID(unsigned long id)
//Sets the bizID member attribute of a Business object. New value is to be passed in via 'id'
//parameter. ID numbers can only assume positive integer values of up to 9 digits long.
{
   (this->bizID) = id;
}

//==============================================================================================
void Business::setDebt(float debt)
//Sets the bizDebt member attribute of a Business object. New value is to be passed in via
//'debt' parameter. Maximum amount a business can owe is $9999.99. A business may not owe a
//negative debt.
{
   (this->bizDebt) = debt;
}

//==============================================================================================
strstream & operator<<(strstream &os, const Business &printMe)
//Overload of the << operator. Enables output of a Business object to an output stream the same
//way a string can be outputed via '<<'.
{
   //print Business ID number in field of width 9 and right justified.
   os.width(9);
   os << printMe.getID();

   //print blank space followed by dollar sign in a field of length 24 and right justified.
   os.width(24);
   os << "$";

   //print business' owing debt in...
   os.width(7); //a field of length 7 and right justified,
   os.precision(2); //to 2 decimal places,
   os.setf(ios_base::fixed, ios_base::floatfield); //using fixed decimal point.
   os << printMe.getDebt() << ends; //print debt with above formatting.

   return os; //return the entire string created by the above statements.
}

//==============================================================================================
void Business::print(ostrstream &os)
//Outputs a Business object to an output stream. Serves the same function as the operator<<(),
//and it also outputs the information in the exact same format. You have to specify where you
//want to output the Business object to by passing in an output stream parameter to the function
//via 'os'.
{
   //print Business ID number in field of width 9 and right justified.
   os.width(9);
   os << this->getID();

   //print blank space followed by dollar sign in a field of length 24 and right justified.
   os.width(24);
   os << "$";

   //print business' owing debt in...
   os.width(7); //a field of length 7 and right justified,
   os.precision(2); //to 2 decimal places,
   os.setf(ios_base::fixed, ios_base::floatfield); //using fixed decimal point.
   os << this->getDebt() << ends; //print debt with above formatting.
}

//==============================================================================================
Business * Business::makePtrToClone()
//Creates a duplicate of the Business object that calls this function by using copy constructor
//of this class. At the end, it returns a pointer to the newly created Business object.
{
   return ( new Business(*this) );
}


//##############################################################################################
//                          MonthlyPayingBusiness Class Implementation
//##############################################################################################


//==============================================================================================
unsigned long MonthlyPayingBusiness::instanceCount = 0;
//Initializes the static variable, instanceCount, to 0 before any MonthyPayingBusiness objects
//are created. Used to keep track of how many instances of MonthlyPayingBusiness objects exist.

//==============================================================================================
MonthlyPayingBusiness::MonthlyPayingBusiness()
//Default constructor for MonthlyPayingBusiness class. Initializes business' ID number, debt,
//and monthly payments to 0. Then it increments the static variable, instanceCount, defined
//in the MonthlyPayingBusiness class, and decrements the variable by the same name in the
//Business class. This is necessary because MonthlyPayingBusiness is also of type Business.
{
   monthlyPayment = 0;
   MonthlyPayingBusiness::instanceCount++;
   Business::instanceCount--;
}

//==============================================================================================
MonthlyPayingBusiness::MonthlyPayingBusiness(const MonthlyPayingBusiness &cloneMe)
//Copy contructor. Creates a duplicate of the MonthlyPayingBusiness object passed in via
//'cloneMe' parameter. It also increments the static variable, instanceCount, since a new
//MonthlyPayingBusiness object is being created and decrements the variable by the same in
//Business class. This is necessary because MonthlyPayingBusiness is also of type Business.
//Then it automatically returns a pointer to the newly created MontlyPayingBusiness object.
{
   monthlyPayment = cloneMe.monthlyPayment;
   //NOTE TO SELF:
   //This will differentiate between the 2 instanceCounts, but it will still increment both
   //due to the 'chaining' constructors.
   MonthlyPayingBusiness::instanceCount++;
   Business::instanceCount--;
}

//==============================================================================================
MonthlyPayingBusiness::~MonthlyPayingBusiness()
//Destructor. Automatically called whenever deallocating the memory being occupied by a
//MonthlyPayingBusiness object. It also decrements the static variable, instanceCount, and
//increments the variable by the same name in the Business class. This is necessary because
//MonthlyPayingBusiness is also of type Business.
{
   MonthlyPayingBusiness::instanceCount--;
   Business::instanceCount++;
}

//==============================================================================================
unsigned long MonthlyPayingBusiness::getInstanceCount()
//Static function that returns the number of MonthlyPayingBusiness objects that currently exist
//as as unsigned long value.
{
   return instanceCount;
}

//==============================================================================================
float MonthlyPayingBusiness::getMonthlyPayment() const
//Returns the monthly payment being made by the business as a float value.
{
   return (this->monthlyPayment);
}

//==============================================================================================
void MonthlyPayingBusiness::setMonthlyPaymentTo(float amount)
//Sets the monthlyPayment member attribute of a MonthlyPayingBusiness object. New value is to be
//passed in via 'amount' parameter. Maximum amount a business can pay is $9999.99.
{
   (this->monthlyPayment) = amount;
}


//==============================================================================================
void MonthlyPayingBusiness::print(ostrstream &os)
//Outputs a MonthlyPayingBusiness object to an output stream. You have to specify where you
//want to output the MonthlyPayingBusiness object to by passing in an output stream parameter to
//the function via 'os'.
{
   //print Business ID number in field of width 9 and right justified.
   os.width(9);
   os << this->getID();

   //print blank space followed by dollar sign in a field of length 24 and right justified.
   os.width(24);
   os << "$";

   //print business' owing debt in...
   os.width(7); //a field of length 7 and right justified,
   os.precision(2); //to 2 decimal places,
   os.setf(ios_base::fixed, ios_base::floatfield); //using fixed decimal point.
   os << this->getDebt(); //print debt with above formatting.

   //print blank space followed by dollar sign in a field of length 3 and right justified.
   os.width(3);
   os << "$";

   //print business' monthly payments in...
   os.width(7); //a field of length 7 and right justified,
   os.precision(2); //to 2 decimal places,
   os.setf(ios_base::fixed, ios_base::floatfield); //using fixed decimal point.
   os << this->getMonthlyPayment() << ends; //print debt with above formatting.
}

//==============================================================================================
Business * MonthlyPayingBusiness::makePtrToClone()
//Creates a duplicate of the MonthlyPayingBusiness object that calls this function by using copy
//constructor of this class. At the end, it returns a pointer to the newly created Business
//object.
{
   return ( new MonthlyPayingBusiness(*this) );
}
