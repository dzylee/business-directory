//file: bizlist.h
//Templatized BizList class. The class declaration and implementations follow below, in that
//order. Provides public interfaces for creating a container to store a list of type Business,
//and all its subclasses.


/*
(David) Zeng-Yuan LEE
Course: CMPT 212
Assignment #: 5
Date: April 4, 2003
*/


#ifndef _bizlist_
#define _bizlist_


#include <cstddef>


//##############################################################################################
template <class T> class BizList
//BizList class declaration.
{

public:


//==============================================================================================
BizList();
//Default constructor. Creates an empty BizList object for storing Business objects.

//==============================================================================================
BizList(const BizList &cloneMe);
//Copy contructor. Create a copy of a BizList object. The object to be copied to is passed in to
//the function as a parameter via 'cloneMe'.

//==============================================================================================
~BizList();
//Destructor. Automatically called whenever the delete operator is used on a BizList object. It
//destroys all Business objects held in the BizList.

//==============================================================================================
unsigned long getNodeCount() const;
//Returns the total number of businesses currently stored in the BizList object.

//==============================================================================================
void push_back(T *addMe);
//Inserts a Business object at the back of BizList. The Business object to be inserted is passed
//in as a parameter to the function via 'addMe'.

//==============================================================================================
bool getNext(T *&defineMe);
//Function retrieves the next business record in the linked list and passes back a pointer to
//the business by reference. The calling function must pass in a blank Business object as a
//parameter. If the next business record exists, it is passed back by reference and the function
//returns true. If it doesn't exist, false is returned, and no modifications are made to
//defineMe. Note that getNext() returns the next business relative to where the internal
//iterator is currently pointing to, so if you want to start at the beginning of the list, be
//sure to call reset() first.

//==============================================================================================
void reset();
//Function resets the internal iterator to point back to the first item in the linked list. If
//you want to traverse every item in the linked list, call this function first, then repeatedly
//call getNext().

//==============================================================================================
void empty();
//Function empties the linked list of all items that are currently stored in it.

//==============================================================================================
const BizList & operator=(const BizList &copyMe);
//Overload of the of = operator. Enables a deep copy of one BizList to another using the
//assignment operator. (eg: BizList1 = BizList2). Note that this assignment operator only copies
//over objects of type Business, and nothing else that is a subclass of Business, which means
//any MonthlyPayingBusiness objects in the list will not be copied over.


private:


//==============================================================================================
bool isEmpty() const;
//Function determines if the linked list is empty It returns true if the list is empty, and
//false otherwise.

//==============================================================================================
unsigned long nodeCount;
//Stores the number of nodes currently in the BizList object as a unsigned long value.

//==============================================================================================
struct BizNode
//Structure that is internal to the BizList used to create nodes.
{
   T *data; //pointer to a Business object.
   BizNode *nextNode; //pointer to another BizNode object.
};

//==============================================================================================
BizNode *head; //pointer for keeping track of where the first item in the BizList is.
const BizNode *cursor; //pointer used for traversing the items in the BizList.

};


//##############################################################################################
//BizList class implementation. Data type implements a linked list used to store objects of type
//Business.


//==============================================================================================
template <class T> BizList<T>::BizList()
//Default constructor for BizList class. Creates a linked list for containing Business objects.
//Initializes the head pointer and the cursor pointer to NULL since the linked list is
//initially empty. Also initializes the node count for that particular list to 0.
{
   head = NULL;
   cursor = NULL;
   nodeCount = 0;
}

//==============================================================================================
template <class T> BizList<T>::BizList(const BizList &cloneMe)
//Copy constructor. Creates a duplicate of the BizList object passed in via 'cloneMe' parameter.
//It will traverse through the source list and duplicate each node individually over into the
//the new list.
{
   //you're copying an empty list.
   if( cloneMe.head == NULL )
   {
      head = NULL;
      cursor = NULL;
      nodeCount = 0;
   }
   //you're copying a non-empty list.
   else
   {
      BizNode *destNode;
      destNode = new BizNode; //used as empty container to copy over node contents.
      BizNode *sourceNode; //used for traversing the source list.

      //point the sourceNode to the first item in the source list.
      sourceNode = cloneMe.head;

      //begin by copying the first item.
      (destNode->data) = new T( *(sourceNode->data) );

      //set the head and cursor pointers to point to the first item for this new list.
      head = destNode;
      cursor = destNode;

      //copy the rest of the nodes in the list.
      while( (sourceNode->nextNode) != NULL ) //execute loop body if there are more nodes.
      {
         (destNode->nextNode) = new BizNode; //allocate new node at back end of new list.
         destNode = destNode->nextNode; //point destNode to the newly allocated node.

         sourceNode = sourceNode->nextNode; //point sourceNode to next node to copy.

         (destNode->data) = new T( *(sourceNode->data) );
      }
      destNode->nextNode = NULL; //set 'nextNode' of the last node to point to NULL;
      nodeCount = cloneMe.getNodeCount(); //update the new list's node count.
   }
}

//==============================================================================================
template <class T> BizList<T>::~BizList()
//Destructor. Automatically called whenever deallocating the memory being occupied by a BizList
//object. See empty() at bottom for details of the algorithm used.
{
   this->empty();
}

//==============================================================================================
template <class T> unsigned long BizList<T>::getNodeCount() const
//Returns the number of the nodes currently in the list as an unsigned long value.
{
   return (this->nodeCount);
}

//==============================================================================================
template <class T> bool BizList<T>::isEmpty() const
//Determines whether list is empty by checking if the head pointer is pointing to NULL. Returns
//true if the list is empty, and false otherwise.
{
   return (head == NULL);
}

//==============================================================================================
template <class T> void BizList<T>::push_back(T *addMe)
//Inserts a Business object at the back of the list. Algorithm will find the end of the list
//through iterative searching. The business record to be inserted is passed into the function
//via 'addMe' parameter.
{
   //allocate a new node.
   BizNode *newNode;
   newNode = new BizNode;

   //encase the business to be added into the newly created node.
   newNode->data = addMe;
   newNode->nextNode = NULL; //since it's added to the back, the list has no 'next' node.

   //now add the node into the linked list.
   if( this->isEmpty() ) //the list is empty, so the node will be the first in the list.
   {
      head = newNode;
      cursor = newNode;
   }
   //list is not empty.
   else
   {
      //create a new, temporary iterator, so as to not interfere with the cursor pointer.
      BizNode *iterator;
      iterator = head;

      //iteratively search for the end of the list to add the node at.
      while( true )
      {
         //determine if you are at the end of the list by checking if interator's
         //nextNode field is pointing to NULL.
         if( (iterator->nextNode) == NULL )
         {
            (iterator->nextNode) = newNode; //if so, add the new node at the back and
break; //break out of the loop to exit the function.
         }
         //iterator is not pointing to the last node.
         else
         {
            iterator = (iterator->nextNode); //move on to the next node.
         }
      }
   }
   nodeCount++; //increment the node count for this list.
}

//==============================================================================================
template <class T> bool BizList<T>::getNext(T *&defineMe)
//Function retrieves the next business record following the cursor pointer, passes it back
//by reference via the &defineMe parameter, and then the function returns true. Prior to
//returning, cursor is pointed to the next node in the list and the function is ready for the
//next call. If the next business record doesn't exist, defineMe is left unchanged and the
//function returns false. The cursor pointer will not be updated prior to returning.
{
   if( cursor == NULL ) //check if past the end of the list.
   {
return false; //reached past the end of the list. No nodes left, return false.
   }
   else //a 'next' node does exist.
   {
      defineMe = (cursor->data); //pass the next Business object back by reference.
      cursor = (cursor->nextNode); //point cursor to the next node.
return true; //successful getNext(), return true.
   }
}

//==============================================================================================
template <class T> void BizList<T>::reset()
//Resets the cursor pointer by pointing cursor back to the front of the linked list.
{
   cursor = head;
}

//==============================================================================================
template <class T> void BizList<T>::empty()
//Empties the linked list of all its nodes, and resets the head and cursor pointers back to
//NULL.
{
   //emptying a list that's already empty.
   if ( head == NULL )
   {
      cursor = NULL;
   }
   //emptying a list that has items in it.
   else
   {
      do
      {
         //use cursor as the current-node pointer and use head as the next-node pointer.
         cursor = head;
         head = (head->nextNode);

         //deallocate the memory.
         delete (cursor->data);
      }
      while( head != NULL ); //execute the loop body again, if there are more nodes left.

      //once all nodes have been deleted, reset all the member attributes.
      head = NULL;
      cursor = NULL;
      nodeCount = 0;
   }
}

//==============================================================================================
template <class T> const BizList<T> & BizList<T>::operator=(const BizList &copyMe)
//Overload of the of = operator. Enables a deep copy of one BizList to another using the
//assignment operator.
{
   if( this != &copyMe ) //make sure the BizList isn't being copied over onto itself.
   {
      //empty 'this'.
      empty(); //empty the destination BizList.

      //you're copying an empty list.
      if( copyMe.head == NULL )
      {
         head = NULL;
         cursor = NULL;
         nodeCount = 0;
      }
      //you're copying a non-empty list.
      else
      {
         BizNode *destNode = new BizNode; //used as container to copy over node contents.
         BizNode *sourceNode; //used for traversing the source list.

         //counter to keep track of how many Business objects are copied over.
         unsigned long bizCount = 0;

         //point the sourceNode to the first item in the source list.
         sourceNode = copyMe.head;

         //begin by finding the first Business object in the list, and copy it over if it
         //exists.
         while( true )
         {
            //use RTTI to determine if current node holds a Business object.
            if( typeid( *(sourceNode->data) ) == typeid(T) )
            {
               //it does contain a Business object. Copy it over.
               (destNode->data) = ( sourceNode->data->makePtrToClone() );

               //set the head and cursor pointers to point to the first item for this new
               //list.
               head = destNode;
               cursor = destNode;
               bizCount++; //increment counter
               break; //first Business has been found and copied, break out of loop.
            }
            //check if there are any nodes left in the list to search through.
            else if( sourceNode->nextNode == NULL )
            {
               break; //break out of the loop if there are no more nodes.
            }
            //there are more nodes to search through.
            else
            {
               sourceNode = sourceNode->nextNode; //point sourceNode to next node to copy.
            }
         }

         //Copy over the rest of the Business objects in the list if there are any.
         while( (sourceNode->nextNode) != NULL ) //execute loop body if there are more nodes.
         {
            sourceNode = sourceNode->nextNode; //point sourceNode to next node to copy.

            //use RTTI to determine if current node holds a Business object.
            if( typeid( *(sourceNode->data) ) == typeid(T) )
            {
                //allocate new node at back end of new list.
               (destNode->nextNode) = new BizNode;
               destNode = destNode->nextNode; //point destNode to the newly allocated node.

               (destNode->data) = ( sourceNode->data->makePtrToClone() );   //copy it.
               bizCount++; //increment the counter.
            }
         }
         destNode->nextNode = NULL; //set 'nextNode' of the last node to point to NULL;
         nodeCount = bizCount; //update the new list's node count.
      }
   }
   return *this; //return the copied object
}


#endif
