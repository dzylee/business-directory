//file: control.h
//This file contains prototypes of functions that provide interfaces for interacting with a
//business directory that keeps track of how much debt a business owes.


/*
(David) Zeng-Yuan LEE
Course: CMPT 212
Assignment #: 5
Date: April 4, 2003
*/


#ifndef _control_
#define _control_


class BizList; //forward declaration.


//==============================================================================================
void displayList(BizList<Business> &showMe, CPaintDC &drawOnMe);
//Function prints to screen all the business records stored in the BizList linked list. It
//displays 2 totals at the end. One for the number of Business objects in the list, and the
//other for the number of MonthPayingBusiness objects.


#endif
