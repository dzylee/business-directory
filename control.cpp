//file: control.cpp
//Implements functions that provide interfaces for interacting with a business directory that
//keeps track of how much debt a business owes.


/*
(David) Zeng-Yuan LEE
Course: CMPT 212
Assignment #: 5
Date: April 4, 2003
*/


#include <afxwin.h>

#include <iostream>
using namespace std;

#include "business.h"
#include "bizlist.h"
#include "control.h"


//==============================================================================================
void displayList(BizList<Business> &showMe, CPaintDC &drawOnMe)
//Function prints to window all the business records stored in the BizList linked list. It
//displays the total number of records in the list at the end of its output.
{
   showMe.reset(); //call the reset function to ensure iterator in the linked list is pointing
               //to the first item in the list.

   //print the headings for the table.
   drawOnMe.TextOut( 10, 30, "DEBTOR ID                       AMOUNT    MONTHLY " );

   long bizCount = 0; //temporary variable to store the business count.
   Business *bizInfo; //temporary business pointer for retrieving each business' information.

   //create a new style I/O variable for outputing to the main window client area.
   static char line[80];
   static ostrstream winOut( line, sizeof(line) );

   //iteratively retrieve each business record.
   while( showMe.getNext(bizInfo) ) //execute body of loop if there exists more records.
   {
      winOut.seekp(0);
      bizInfo->print(winOut); //display the business' information.
      drawOnMe.TextOut( 10, 50+(bizCount*20), line );
      bizCount++; //increment the temporary counter.
   }

   winOut.seekp(0);

   //display number of businesses using the iterative method.
   winOut << "The total number of businesses is " << bizCount << ". (iterative method)"
      << ends;
   drawOnMe.TextOut( 10, 50+(bizCount+1)*20, line ); //display the line.

   winOut.seekp(0);

   //display the number of business using the static instance method.
   winOut << "The total number of businesses is " << showMe.getNodeCount() <<
      ". (static instance method)" << ends;
   drawOnMe.TextOut( 10, 50+(bizCount+2)*20, line ); //display the line.

}
